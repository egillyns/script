#!/bin/bash

    processes=$(ps -e -o pid,pcpu,pmem,etimes,etime,user,args --sort -args,user,pcpu,pmem)
    cpu=($(echo "$processes" | awk '($2>50) { print int(($4/(60))+0.5), $2 $4 }' | awk -f ~/script/uniq.awk | awk '{print $1}'))
    timerun=($(echo "$processes" | awk '($2>50) { print int(($4/(60))+0.5), $2 $4 }' | awk -f ~/script/uniq.awk | awk '{print $2}'))
#no argument
if [ $# -eq 0 ]; then
    echo "CPU, PID, CPU%, MEM%, time, user, application, directory"
    cpu_tot=0
    for (( i=0; i<=(${#cpu[@]}-1); i++ ))
    do
        output[i]=$(echo "$processes" | awk '($2>50) { print int(($4/(60))+0.5), $0 }' | awk -v var=${timerun[$i]} '{if ($1 == var) {print $2, $3, $4, $6, $7, $8; exit;}}' )
        PID[i]=$(echo ${output[i]} | awk '{print $1}')
        DIR[i]=$(pwdx ${PID[i]} 2>/dev/null | grep -o '/[^\/]*/[^\/]*$')
        echo ${cpu[$i]} ${output[i]} ${DIR[i]}
        ((cpu_tot+=${cpu[$i]}))
    done
    cores=$(lscpu | sed -n '9p' | awk '{print $NF}')
    soc_per_cor=$(lscpu | sed -n '8p' | awk '{print $NF}')
    echo "CPU cores used : $cpu_tot / $(($cores * $soc_per_cor))"
fi

#argument to kill a process
if [[ $1 == 'kill' ]]
then
    for (( i=0; i<=(${#cpu[@]}-1); i++ ))
    do
        output[i]=$(echo "$processes" | awk '($2>50) { print int(($4/(60))+0.5), $0 }' | awk -v var=${timerun[$i]} '{if ($1 == var) {print $2, $3, $4, $6, $7, $8; exit;}}' )
        PID[i]=$(echo ${output[i]} | awk '{print $1}')
        DIR[i]=$(pwdx ${PID[i]} 2>/dev/null | grep -o '/[^\/]*/[^\/]*$')
        echo $i ${cpu[$i]} $(echo ${output[i]} | awk '{print $5, $6}') ${DIR[i]}
    done
    echo "Enter the number of the process to kill :"
    read var1
    read -p "Killing $(echo ${output[var1]} | awk '{print $5, $6}') ${DIR[var1]}, continue (y/n)?" choice
    case "$choice" in 
        y|Y ) kill ${PID[var1]} && echo "Killed the process";;
        n|N ) echo "Abort";;
        * ) echo "invalid";;
    esac
fi

