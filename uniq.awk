################################################################################
# File name: uniq.awk
# ===================
#
# Find and report all occurrences of duplicate lines in a text file.
#
#
# Usage: awk -f uniq.awk [FILE]
#
################################################################################
{
    x = lines[$1]["count"]++; # Count the number of occurrences of a line
    lines[$0]["NR"][x] = NR;  # Also save the number lines
    
    # Find the length of the longest line to make it the column width
    if (x > 0) {
        if (length($0) > max) {
            max = length($0);
        }
    }
}
END {
    # If the file contains no lines to process, that is, it's empty,
    # return an exit status code of 1 to indicate the fact.
    if (!(NR > 0)) {
        exit 1;
    }
        
    # Column #1: number of occurrences of the line
    # Column #2: line itself
    
    for (i in lines) {
        if (lines[i]["count"] > 1) {
            #print(lines[i]["count"], "CPU used by :", i);
            print(lines[i]["count"], i);
            #print(i);
        }
    }
}
